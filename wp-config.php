<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'climatechange' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'AQ]#pg]2!SsNEeQj+Sh7*CXg#R]W3R$pVs!1Y%SO&V[?f>:TC9w6R+R?qrlU9lCM' );
define( 'SECURE_AUTH_KEY',  'Z!Q5ILDrG+( AkM7vo*jF.2Q1;6pITRF4jN/LIm9S*s#e~E/>`AFauR&|G2Z)L{-' );
define( 'LOGGED_IN_KEY',    'D|/ts@U9nA>~f+*?Mcc[lr=PqLos>$0r@}wCmiHhJ)rFM+*0(_ @$_T1:osQs|z8' );
define( 'NONCE_KEY',        'ydgalfJz bqVLawEUIk)MzyKj5&$c2F(m$Bv#0?wVC<7`}T$!dIqDH!ve6Ufx]D`' );
define( 'AUTH_SALT',        'fn!225Kuz^pmpH>&F4BBTzCqT<q<_j?v*I(7X:7Yfb8%>]HBV+{(,>piI)7d252a' );
define( 'SECURE_AUTH_SALT', '|l;@+:mQ}^b}7W;J,fcVJZ{(r3elrIvM@=[cMJ ?E3-tG#/RS[%!~f;){vKA|&a$' );
define( 'LOGGED_IN_SALT',   'pNVBB36b1k.xOW&=JV|UK!vO&D>T+)TqTml)rpWk 5 b%4{ RX!~Cj2M](|8FgP(' );
define( 'NONCE_SALT',       'B3z+ZyiR[_~)}N?LZ*|ApGmpa*?xFVz%+rs3nR&Gkx6Q3RH-|DR&:*f7gjsC$!A ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
